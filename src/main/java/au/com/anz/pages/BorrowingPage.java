package au.com.anz.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BorrowingPage extends BasePage {

    @FindBy(xpath = "//select[@title='Number of dependants']")
    private WebElement ddlnoOfDependents;

    @FindBy(xpath = "//label[contains(text(), 'Your income (before tax)')]/following-sibling::div/input")
    private WebElement txtIncome;

    @FindBy(xpath = "//label[contains(text(), 'Your other income')]/following-sibling::div/input")
    private WebElement txtOtherIncome;

    @FindBy(xpath = "//label[contains(text(), 'Living expenses')]/following-sibling::div/input")
    private WebElement txtLivingExpenses;

    @FindBy(xpath = "//label[contains(text(), 'Current home loan repayments')]/following-sibling::div/input")
    private WebElement txtCurrentHomeLoanRepayments;

    @FindBy(xpath = "//label[contains(text(), 'Other loan repayments')]/following-sibling::div/input")
    private WebElement txtOtherLoanRepayments;

    @FindBy(xpath = "//label[contains(text(), 'Other commitments')]/following-sibling::div/input")
    private WebElement txtOthercommitments;

    @FindBy(xpath = "//label[contains(text(), 'Total credit card limits')]/following-sibling::div/input")
    private WebElement txtTotalCreditCardLimits;

    @FindBy(id = "btnBorrowCalculater")
    private WebElement btnBorrowCalculator;

    @FindBy(id = "borrowResultTextAmount")
    private WebElement lblborrowAmount;

    @FindBy(xpath = "//span[@aria-live='assertive']")
    private WebElement lblAmountDisplayed;

    @FindBy(className = "start-over")
    private WebElement btnStartOver;

    @FindBy(xpath = "//label[@for='application_type_single']/parent::li")
    private WebElement radSingle;

    @FindBy(xpath = "//label[@for='borrow_type_home']/parent::li")
    private WebElement radHome;

    @FindBy(className = "borrow__error__text")
    private WebElement lblErrorMessage;

    public void navigateToBorrowingPage(){
        open();
    }

    public void calculateBorrowingAmount(String applicationType, String noOfDependents, String propertyToBuy,
                                         String income, String otherIncome, String livingExpense,
                                         String currentHomeLoanRepayments, String otherLoanRepayments,
                                         String otherCommitments, String totalCreditCardLimits){

        $("//input[@id='application_type_"+applicationType.toLowerCase()+"']").click();
        selectFromDropdown(ddlnoOfDependents, noOfDependents);
        $("//label[contains(text(), '"+propertyToBuy+"')]/input").click();
        typeInto(txtIncome, income);
        typeInto(txtOtherIncome, otherIncome);
        typeInto(txtLivingExpenses, livingExpense);
        typeInto(txtCurrentHomeLoanRepayments, currentHomeLoanRepayments);
        typeInto(txtOtherLoanRepayments, otherLoanRepayments);
        typeInto(txtOthercommitments, otherCommitments);
        typeInto(txtTotalCreditCardLimits, totalCreditCardLimits);
        clickOn(btnBorrowCalculator);
    }

    public String getEstimate(){
        waitFor(lblAmountDisplayed);
        return waitFor(lblborrowAmount).getText().trim();
    }

    public void clickStartOver(){
        waitFor(lblAmountDisplayed);
        clickOn(btnStartOver);
    }

    public boolean isFormCleared(){
        waitFor(btnBorrowCalculator);
        return getTextBoxValue(txtIncome).equals("0") &&
        getTextBoxValue(txtOtherIncome).equals("0") &&
        getTextBoxValue(txtLivingExpenses).equals("0") &&
        getTextBoxValue(txtCurrentHomeLoanRepayments).equals("0") &&
        getTextBoxValue(txtOtherLoanRepayments).equals("0") &&
        getTextBoxValue(txtOthercommitments).equals("0") &&
        getTextBoxValue(txtTotalCreditCardLimits).equals("0") &&
        getSelectedValueFrom(ddlnoOfDependents).equals("0") &&
        isRadioChecked(radSingle) &&
        isRadioChecked(radHome);
    }

    public String getErrorMessage(){
        waitFor(lblErrorMessage);
        return lblErrorMessage.getText().trim();
    }
}
