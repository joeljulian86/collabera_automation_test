package au.com.anz.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BasePage extends PageObject {

    public String getTextBoxValue(WebElement webElement){
        return webElement.getAttribute("value");
    }

    public boolean isRadioChecked(WebElement webElement){
        return webElement.getAttribute("class").equals("selected");
    }
}
