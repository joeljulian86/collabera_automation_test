package au.com.anz.transformers;

import au.com.anz.mappers.Borrow;
import io.cucumber.core.api.TypeRegistry;
import io.cucumber.core.api.TypeRegistryConfigurer;
import io.cucumber.datatable.DataTableType;
import io.cucumber.datatable.TableEntryTransformer;

import java.util.Locale;
import java.util.Map;

public class Transformer implements TypeRegistryConfigurer {


    @Override
    public Locale locale() {
        return Locale.ENGLISH;
    }

    @Override
    public void configureTypeRegistry(TypeRegistry typeRegistry) {
        typeRegistry.defineDataTableType(new DataTableType(Borrow.class, new TableEntryTransformer<Borrow>() {
            @Override
            public Borrow transform(Map<String, String> details) throws Throwable {
                return new Borrow(details.get("Application Type"),
                        details.get("Number of dependents"),
                        details.get("Property you would like to buy"),
                        details.get("Your income (before tax)"),
                        details.get("Your other income"),
                        details.get("Living expenses"),
                        details.get("Current home loan repayments"),
                        details.get("Other loan repayments"),
                        details.get("Other commitments"),
                        details.get("Total credit card limits"));
            }
        }));
    }
}
