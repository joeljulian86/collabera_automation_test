package au.com.anz.mappers;

public class Borrow {

    private String applicationType;
    private String noOfDependents;
    private String propertyToBuy;
    private String income;
    private String otherIncome;
    private String livingExpense;
    private String currentHomeLoanRepayments;
    private String otherLoanRepayments;
    private String otherCommitments;
    private String totalCreditCardLimits;

    public Borrow(String applicationType, String noOfDependents, String propertyToBuy, String income, String otherIncome, String livingExpense, String currentHomeLoanRepayments, String otherLoanRepayments, String otherCommitments, String totalCreditCardLimits) {
        this.applicationType = applicationType;
        this.noOfDependents = noOfDependents;
        this.propertyToBuy = propertyToBuy;
        this.income = income;
        this.otherIncome = otherIncome;
        this.livingExpense = livingExpense;
        this.currentHomeLoanRepayments = currentHomeLoanRepayments;
        this.otherLoanRepayments = otherLoanRepayments;
        this.otherCommitments = otherCommitments;
        this.totalCreditCardLimits = totalCreditCardLimits;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public String getNoOfDependents() {
        return noOfDependents;
    }

    public String getPropertyToBuy() {
        return propertyToBuy;
    }

    public String getIncome() {
        return income;
    }

    public String getOtherIncome() {
        return otherIncome;
    }

    public String getLivingExpense() {
        return livingExpense;
    }

    public String getCurrentHomeLoanRepayments() {
        return currentHomeLoanRepayments;
    }

    public String getOtherLoanRepayments() {
        return otherLoanRepayments;
    }

    public String getOtherCommitments() {
        return otherCommitments;
    }

    public String getTotalCreditCardLimits() {
        return totalCreditCardLimits;
    }
}
