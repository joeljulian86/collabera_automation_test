package au.com.anz.stepdefinitions;

import au.com.anz.mappers.Borrow;
import au.com.anz.pages.BorrowingPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class BorrowSteps {

    @Steps
    BorrowingPage borrowingPage;

    @Given("User is on borrow page")
    public void userIsOnBorrowPage() {
        borrowingPage.navigateToBorrowingPage();
    }

    @When("User enters the below details")
    public void userEntersTheBelowDetails(List<Borrow> details){
        for(Borrow detail : details) {
            borrowingPage.calculateBorrowingAmount(detail.getApplicationType(),
                    detail.getNoOfDependents(),
                    detail.getPropertyToBuy(),
                    detail.getIncome(),
                    detail.getOtherIncome(),
                    detail.getLivingExpense(),
                    detail.getCurrentHomeLoanRepayments(),
                    detail.getOtherLoanRepayments(),
                    detail.getOtherCommitments(),
                    detail.getTotalCreditCardLimits());
        }
    }

    @Then("User should have a borrowing estimate of {string}")
    public void userShouldHaveABorrowingEstimateOf(String expectedEstimate){
        String actualEstimate = borrowingPage.getEstimate();
        Assert.assertEquals(expectedEstimate, actualEstimate);
    }

    @And("User clicks start over button")
    public void userClickStartOverButton(){
        borrowingPage.clickStartOver();
    }

    @Then("The form should get cleared")
    public void theFormShouldGetCleared(){
        Assert.assertTrue(borrowingPage.isFormCleared());
    }

    @Then("Below error message should be displayed")
    public void belowErrorMessageIsDisplayed(String expectedErrorMessage){
        String actualErrorMessage = borrowingPage.getErrorMessage();
        Assert.assertEquals(expectedErrorMessage, actualErrorMessage);
    }
}
