Feature: Calculator and tools
  As a potential customer, I would like to know my borrowing capacity

  Background:
    Given User is on borrow page

  Scenario: Verifying borrowing estimate is calculated correctly
    When User enters the below details
    |Application Type|Number of dependents|Property you would like to buy|Your income (before tax)|Your other income|Living expenses|Current home loan repayments|Other loan repayments|Other commitments|Total credit card limits|
    |Single          |0                   |Home to live in               |80000                   |10000            |500            |0                           |100                  |0                |10000                   |
    Then User should have a borrowing estimate of "$507,000"


  Scenario: Verify clicking the ‘start over’ button clears the form.
    When User enters the below details
      |Application Type|Number of dependents|Property you would like to buy|Your income (before tax)|Your other income|Living expenses|Current home loan repayments|Other loan repayments|Other commitments|Total credit card limits|
      |Single          |0                   |Residential investment        |80000                   |10000            |500            |0                           |100                  |0                |10000                   |
    And User clicks start over button
    Then The form should get cleared

  Scenario: Verify error message is displayed when only Living expenses are entered
    When User enters the below details
      |Application Type|Number of dependents|Property you would like to buy|Your income (before tax)|Your other income|Living expenses|Current home loan repayments|Other loan repayments|Other commitments|Total credit card limits|
      |Single          |0                   |Residential investment        |0                       |0                |1              |0                           |0                    |0                |0                       |
    Then Below error message should be displayed
      """
      Based on the details you've entered, we're unable to give you an estimate of your borrowing power with this calculator. For questions, call us on 1800 035 500.
      """