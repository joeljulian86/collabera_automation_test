Pre-requisites:
1. Java 8+ installed and configured
2. Maven configured successfully

To execute the project, navigate to the project at the terminal enter the below command:
mvn clean verify -Denvironment=default

The results can be found at the following location:
target/site/serenity/index.html